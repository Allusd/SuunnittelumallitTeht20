import java.util.Iterator;
/**
 *
 * @author aleks
 */
import java.util.NoSuchElementException;


public class Array<T> implements Iterable<T> {
    T[] values;  // this contains the actual elements of the array

    // Constructor that takes a "raw" array and stores it
    public Array(T[] values) {
        this.values = values;
    }


    class ArrayIterator implements Iterator<T> {
        int current = 0;  // the current element we are looking at

        // return whether or not there are more elements in the array that
        // have not been iterated over.
        public boolean hasNext() {
            if (current < Array.this.values.length) {
                return true;
            } else {
                return false;
            }
        }

        // return the next element of the iteration and move the current
        // index to the element after that.
        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            return values[current++];
        }
    }

    // Return the value
    public T get(int index) {
        return values[index];
    }

    // Set the value 
    public void set(int index, T value) {
        values[index] = value;
    }

    // Return the length
    public int length() {
        return values.length;
    }

    // Return an iterator over the elements in the array. 
    public Iterator<T> iterator() {
        return new ArrayIterator();
    }

    public static void main(String[] args) {
        // create an array of strings
        String[] strings = new String[]{"Hello", "World"};

        // create a new array to hold these strings
        Array<String> array = new Array<String>(strings);

        // get and print the first values (prints "Hello")
        System.out.println(array.get(0));

        // set the second value
        array.set(1, "Javaland!");

        // iterate over the array, printing "Hello\nJavaland!"
        for (String s : array) {
            System.out.println(s);
        }
    }
}